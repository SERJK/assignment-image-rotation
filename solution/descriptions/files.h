#ifndef ROTATION_IO_FORMATS_H
#define ROTATION_IO_FORMATS_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_FILE_NOT_FOUND,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    INVALID_PATH
};
enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};
enum open_status  {
    OPEN_OK = 0,
    OPEN_ERROR
};
enum close_status  {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum file_mods {
    WR,
    RD,
    ALL
};

struct bmp_header create_header(struct image const *img);

enum open_status file_input_open(const char *input, FILE** filename, const char *mode);

enum close_status file_input_close(FILE** filename);

enum read_status from_bmp(FILE* filename, struct image* img);

enum write_status to_bmp(FILE* filename, struct image const* img);

#endif
