#ifndef ROTATION_ROTATE_H
#define ROTATION_ROTATE_H

#include "image.h"


struct image rotate(struct image const* old_image);
#endif
