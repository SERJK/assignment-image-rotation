#ifndef ROTATION_IMAGE_H
#define ROTATION_IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

void set_image_size(struct image* image, uint64_t height, uint64_t width);

struct image image_create(uint64_t width, uint64_t height);

uint64_t get_padding(uint64_t width);

uint64_t get_size(uint64_t height, uint64_t width);

#endif
