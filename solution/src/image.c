#include "../descriptions/image.h"
#include <malloc.h>

struct image image_create(uint64_t width, uint64_t height) {
    struct image img = {0};
    set_image_size(&img, height, width);
    return img;
}

void set_image_size(struct image* image, uint64_t height, uint64_t width) {
    image->height = height;
    image->width = width;
    image->data = malloc(height * width * sizeof(struct pixel));
}

uint64_t get_size(uint64_t height, uint64_t width) {
    uint32_t padding = (4 - (width*3 % 4)) % 4;
    return height * (width * sizeof(struct pixel) + padding);
}

uint64_t get_padding(uint64_t width) {
    uint64_t padd = (4 - width * 3 % 4) % 4;
    return padd;
}
