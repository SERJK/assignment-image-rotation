#include "../descriptions/files.h"
#include "../descriptions/rotate.h"
#include <malloc.h>

const char* const  mods[] = {
        [WR] = "wb",
        [RD] = "rb",
        [ALL] = "r+b",
};

int main(int argc, char **argv) {
    (void) argc;
    (void) argv;

    const char *input = argv[1];
    const char *output = argv[2];

    FILE *input_file;
    FILE *output_file;

    struct image old_image;
    struct image new_image;

    struct image i = {234, 435};
    i.height = 2;

    if (argc != 3) {
        printf("Arguments error");
        return -1;
    }

    enum open_status openStatus = file_input_open(input, &input_file, mods[RD]);
    if (openStatus != OPEN_OK)
        return -2;

    enum read_status readStatus = from_bmp(input_file, &old_image);
    if (readStatus != READ_OK)
        return -3;

    enum close_status closeStatus = file_input_close(&input_file);
    if (closeStatus != CLOSE_OK)
        return -4;

    new_image = rotate(&old_image);

    enum open_status openStatus_1 = file_input_open(output, &output_file, mods[WR]);
    if (openStatus_1 != OPEN_OK)
        return -5;

    enum write_status writeStatus = to_bmp(output_file, &new_image);
    if (writeStatus != WRITE_OK)
        return -6;

    enum close_status closeStatus_1 = file_input_close(&output_file);
    if (closeStatus_1 != CLOSE_OK)
        return -7;

    free(old_image.data);
    free(new_image.data);

    return 0;
}
