#include "../descriptions/files.h"
#include <string.h>
#include <unistd.h>

struct bmp_header create_header(struct image const *img) {
    struct bmp_header header = {0};
    header.bfType = 0x4D42;
    header.biSize = 40;
    header.biPlanes = 1;
    header.bfileSize = get_size(img->height, img->width);
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biBitCount = 24;
    header.bOffBits = sizeof(struct bmp_header);
    return header;
}

enum open_status file_input_open(const char *input, FILE** filename, const char *mode) {
    enum open_status status;
    if (strcmp(mode, "wb") == 0 || access(input, F_OK) == 0) {
        *filename = fopen(input, mode);
        status = OPEN_OK;
    } else {
        status = OPEN_ERROR;
    }
    return status;
}

enum close_status file_input_close(FILE** filename) {
    enum close_status status;
    if (!filename)
        status = CLOSE_ERROR;
    else {
        fclose(*filename);
        status = CLOSE_OK;
    }
    return status;
}

enum read_status from_bmp(FILE* filename, struct image* img) {
    enum read_status status;
    if ((filename == NULL) || (filename == stdin))
        status = INVALID_PATH;
    else {
        struct bmp_header header = {0};
        size_t header_size = fread(&header, sizeof(struct bmp_header), 1, filename);
        if ((header_size == 0) || (ferror(filename) != 0))
            status = READ_INVALID_HEADER;
        else {
            set_image_size(img, header.biHeight, header.biWidth);
            if (img->data == NULL)
                status = READ_INVALID_BITS;
            else {
                int offset = fseek(filename, header.bOffBits, SEEK_SET);
                if ((offset != 0) || (ferror(filename) != 0))
                    status = READ_INVALID_SIGNATURE;
                else {
                    uint32_t padd = get_padding(header.biWidth);
                    for (size_t i = 0; i < img->height; i++) {
                        fread(img->data + i * img->width, sizeof(struct pixel) * header.biWidth, 1, filename);
                        fseek(filename, padd, SEEK_CUR);
                    }
                    if (ferror(filename) != 0)
                        status = READ_INVALID_SIGNATURE;
                    else
                        status = READ_OK;
                }
            }
        }
    }
    return status;
}

enum write_status to_bmp(FILE* filename, struct image const* img) {
    enum write_status status;
    if (!filename)
        status = WRITE_ERROR;
    else {
        struct bmp_header new_header = create_header(img);
        new_header.biWidth = img->width;
        new_header.biHeight = img->height;
        new_header.biSizeImage = get_size(img->height, img->width);

        uint32_t padding = get_padding(img->width);
        char a[] = {0, 0, 0};
        fwrite(&new_header, sizeof(struct bmp_header), 1, filename);
        if (ferror(filename) != 0)
            status = WRITE_ERROR;
        else {
            for (uint32_t i = 0; i < img->height; i++) {
                fwrite(img->data + i*img->width, sizeof(struct pixel), img->width, filename);
                fwrite(a, 1, padding, filename);
            }
            status = WRITE_OK;
        }
    }
    return status;
}
