#include "../descriptions/rotate.h"

struct image rotate(struct image const* old_image) {
    struct image new_image = image_create(old_image->height, old_image->width);
    for (uint32_t i = 0; i < old_image->height; i++) {
        for (uint32_t j = 0; j < old_image->width; j++) {
            new_image.data[j * old_image->height + (old_image->height - 1 - i)] = old_image->data[i * old_image->width + j];
        }
    }
    return new_image;
}
